#include<iostream.h>
#include<conio.h>
#include<graphics.h>
#include<math.h>
#include<dos.h>                                                           // header file contain the sleep function
void vishal(int x1,int y1,int x2,int y2,int v)
{
	while(y1<=y2)
	{
		for(int i=x1;i<=x2;i++)
		{   
			putpixel(i,y1,v);                                             // this function is used to fill the color inside Rect.
		}                                                               
		y1++;
	}
}

void ashoka(int x1,int y1,int r)
{
	for(int i=0;i<=360;i+=15)
	{
		float PI=3.14;
		int a=r*cos(i*PI/180);
		int b=r*sin(i*PI/180);                                           // this function for ashoka chaktra 24 lines
		sleep(1);
		line(x1,y1,x1+a,y1-b);
	}
}
int main()
{

      int gd=DETECT,gm=0;
      initgraph(&gd,&gm," ");
      rectangle(100,100,400,150);
      vishal(100,100,400,150,4);
      rectangle(100,150,400,200);
      vishal(100,150,400,200,15);
      setcolor(BLACK);
      circle(250,175,25);
      rectangle(100,200,400,250);
      vishal(100,200,400,250,2);
      ashoka(250,175,25);
      getch();


}
